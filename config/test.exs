use Mix.Config

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :contact_info, ContactInfo.Repo,
  username: "postgres",
  password: "postgres",
  database: "contact_info_test#{System.get_env("MIX_TEST_PARTITION")}",
  hostname: System.get_env("POSTGRES_HOSTNAME") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

config :contact_info, ContactInfoWeb.Endpoint,
  authentication: %{
    # we want to test that authentication works
    enabled?: true
  }

config :joken,
  rs256: [
    signer_alg: "RS256",
    key_pem: """
    -----BEGIN PUBLIC KEY-----
    MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArKvPhNPbEaygaarCzxWe
    juJD5xehea6rd4bK5Xg0ZMc68VGi811Ri0IYUAlXubMY9CTzsteqwvMrbz6XXPsl
    2cE+9rHg+zCmj70DgpaVr97o8hjsyt1LNEysBYsMOPMQN+wBpDyWxMuFjP6H/Xa6
    BbzQZxR5qvr46OJBpOn7GJdUvU8JQ9q4KS1lb8ufJYjdNR9uekYKu2hNfabHc6+O
    21ZBGxKMMhp02EXNV0W5qS6qKFQ/BLQaOu2Q86pOuLcKbz7OfRGDddkA6P9P0XPR
    9oIYyiVF4UmnmGD8hKxAdUZEYiYSn4lPi1Fex+h332+qtKx4eSSe6z2Y5vuuTIoJ
    lQIDAQAB
    -----END PUBLIC KEY-----
    """
  ]

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :contact_info, ContactInfoWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn
