defmodule ContactInfoWeb.PageController do
  use ContactInfoWeb, :controller

  alias ContactInfo.{Repo, Contact}

  import Ecto.Query, only: [from: 2]

  def index(conn, params) do
    case require_case_id_param(params) do
      {:ok, case_id} ->
        contacts =
          Repo.all(
            from contact in Contact,
              where: contact.case_id == ^case_id
          )

        conn
        |> assign(:case_id, case_id)
        |> assign(:contacts, contacts)
        |> render("index.html")

      {:error, error_payload} ->
        invalid_request(conn, error_payload)
    end
  end

  def create(conn, params) do
    with {:ok, contact_form_data} <- require_param("contact", params),
         {:ok, contact} <- validate_contact_form_data(contact_form_data) do
      Repo.insert(contact)

      conn
      |> redirect(to: Routes.page_path(ContactInfoWeb.Endpoint, :index, case_id: contact.case_id))
    else
      {:error, error_payload} ->
        invalid_request(conn, error_payload)
    end
  end

  def delete(conn, params) do
    with {:ok, contact} <- require_contact_form_param(params),
         :ok <- validate_contact_has_valid_id(contact) do
      # I am using delete_all rather than delete here because I don't want double deletes to be an error
      Repo.delete_all(from contact in Contact, where: contact.id == ^contact.id)

      conn
      |> redirect(to: Routes.page_path(ContactInfoWeb.Endpoint, :index, case_id: contact.case_id))
    else
      {:error, error_payload} ->
        invalid_request(conn, error_payload)
    end
  end

  def edit_get(conn, params) do
    with {:ok, contact_id} <- require_contact_id_param(params) do
      contacts = Repo.all(from contact in Contact, where: contact.id == ^contact_id)

      case contacts do
        [] ->
          error_response(conn, 404)

        [contact] ->
          conn
          |> assign(:contact, Contact.changeset(contact))
          |> render("edit.html")

        _ ->
          error_response(conn, 500)
      end
    else
      {:error, error_payload} ->
        invalid_request(conn, error_payload)
    end
  end

  def edit_put(conn, params) do
    with {:ok, contact} <- require_contact_form_param(params),
         :ok <- validate_contact_has_valid_id(contact) do
      case Repo.all(from contact in Contact, where: contact.id == ^contact.id) do
        [] ->
          error_response(conn, 410)

        [prevContact] ->
          with {:ok, _} <- Repo.update(Contact.changeset(prevContact, Map.from_struct(contact))) do
            conn
            |> redirect(
              to: Routes.page_path(ContactInfoWeb.Endpoint, :index, case_id: contact.case_id)
            )
          end

        _ ->
          error_response(conn, 500)
      end
    else
      {:error, error_payload} ->
        invalid_request(conn, error_payload)
    end
  end

  defp require_contact_id_param(params) do
    with {:ok, contact_id_param} <- require_param("contact_id", params) do
      case Integer.parse(contact_id_param) do
        {contact_id, ""} -> {:ok, contact_id}
        _ -> :contact_id_must_be_integer
      end
    end
  end

  defp validate_contact_has_valid_id(contact) do
    case contact do
      %Contact{id: id} when id != nil ->
        if is_integer(id) do
          :ok
        else
          {:error, :contact_id_must_be_integer}
        end

      _ ->
        {:error, :contact_must_have_id}
    end
  end

  defp require_case_id_param(params) do
    with {:ok, case_id} <- require_param("case_id", params) do
      validate_case_id(case_id)
    end
  end

  defp require_contact_form_param(params) do
    with {:ok, contact_form_data} <- require_param("contact", params) do
      validate_contact_form_data(contact_form_data)
    end
  end

  defp validate_case_id(case_id) do
    with {:ok, ^case_id} <- validate_postgres_string(case_id) do
      if String.length(case_id) == 0 do
        {:error, :case_id_can_not_be_empty}
      else
        {:ok, case_id}
      end
    end
  end

  defp require_param(param, params) do
    case params do
      %{^param => param_value} -> {:ok, param_value}
      _ -> {:error, {:missing_param, param}}
    end
  end

  @postgres_max_string_length 255

  # We need to put strings into postgres so we should verify they're
  # going to be valid for that
  defp validate_postgres_string(string) do
    cond do
      not String.valid?(string) ->
        {:error, :not_a_valid_string}

      not (byte_size(string) <= @postgres_max_string_length) ->
        {:error, {:too_long, string, @postgres_max_string_length}}

      true ->
        {:ok, string}
    end
  end

  # We're repeating some of the validation that Ecto will do
  # anyway, but I think it's probably better to check earlier and check twice
  # than not
  defp validate_contact_form_data(contact_form_data) do
    fields = [:id, :case_id, :title, :first_name, :last_name, :mobile_number, :address]
    # We don't want to directly convert form data to atoms because we don't know what weird things
    # might be in it
    contact =
      fields
      |> Enum.reduce(%{}, fn field, contact ->
        put_if_non_empty(contact, field, contact_form_data[Atom.to_string(field)])
      end)

    if Map.has_key?(contact, :case_id) do
      with {:ok, _} <- validate_case_id(contact[:case_id]),
           {:ok, contact} <- map_contact_id_to_integer(contact) do
        {:ok, struct(Contact, contact)}
      end
    else
      {:error, {:missing_param, "case_id"}}
    end
  end

  defp map_contact_id_to_integer(contact) do
    if Map.has_key?(contact, :id) do
      case Integer.parse(contact.id) do
        {id, ""} -> {:ok, Map.put(contact, :id, id)}
        _ -> {:error, :contact_id_must_be_integer}
      end
    else
      {:ok, contact}
    end
  end

  defp put_if_non_empty(map, field, value) do
    case value do
      nil -> map
      "" -> map
      _ -> Map.put(map, field, value)
    end
  end

  defp error_to_string(error_reason) do
    case error_reason do
      {:missing_param, param} ->
        "Missing required parameter '#{param}'"

      {:too_long, string, max_length} ->
        "Parameter value '#{string}' too long (Max length: #{max_length})"

      :not_a_valid_string ->
        "One parameter was not a valid utf-8 string"

      :case_id_can_not_be_empty ->
        "Parameter case_id can not be empty"

      :contact_id_must_be_integer ->
        "Contact id must be integer"

      :contact_must_have_id ->
        "Contact must have an id"
    end
  end

  defp invalid_request(conn, reason) do
    error_response(conn, 400, message: error_to_string(reason))
  end

  defp error_response(conn, status, assigns \\ []) do
    conn
    |> put_view(ContactInfoWeb.ErrorView)
    |> put_status(status)
    |> render("#{status}.html", assigns)
    |> halt()
  end
end
